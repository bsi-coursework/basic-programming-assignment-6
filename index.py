names = []
phones = []
emails = []

repeat = int(input("Please fill number of entry you want to input `Number`: "))

# Repeat form
for index in range(repeat): 
    print(f"Entry of {index}")
    
    # Get input from user
    name = input("Please input the name: ")
    phone = input("Please input the phone number: ")
    email = input("please input the email: ")

    # Append the input to the list var
    names.append(name)
    phones.append(phone)
    emails.append(email)

# Print table header
print("""NAME     PHONE     EMAIL
-----------------------""")

for index in range(repeat):
    print(f"{names[index]}    {phones[index]}    {emails[index]}")
